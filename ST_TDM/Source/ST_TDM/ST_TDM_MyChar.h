// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"


#include "ST_TDM_MyChar.generated.h"


UCLASS()
class ST_TDM_API AST_TDM_MyChar : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AST_TDM_MyChar();



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "CharAssociatedVariables")
		float MaxHealth = 100;


public:	
	UPROPERTY(Replicated, VisibleAnywhere, Category = "CharAssociatedVariables")
		float CurrentHealth = MaxHealth;

	UPROPERTY(VisibleAnywhere, Category = "CharAssociatedVariables")
		bool CharIsAlive = true;

	UPROPERTY(VisibleAnywhere, Category = "CharAssociatedVariables")
		bool TeamA = true;

	UPROPERTY(VisibleAnywhere, Category = "CharAssociatedVariables")
		bool IsWalking = true;

	UPROPERTY(VisibleAnywhere, Category = "CharAssociatedVariables")
		bool IsRuning = false;

	UPROPERTY(VisibleAnywhere, Category = "CharAssociatedVariables")
		bool IsFiring = false;

	UPROPERTY(VisibleAnywhere, Category = "CharAssociatedVariables")
		bool IsAiming = false;

	float CurrentVelocityX = 0.0f;
	float CurrentVelocityY = 0.0f;

	float CurrentLookX = 0.0f;
	float CurrentLookY = 0.0f;



/////////////////
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Input functions
	void Move_Forward(float AxisValue);
	void Move_Right(float AxisValue);

	void TurnAtRate(float AxisValue);
	float BaseTurnAtRate = 0.0f;

	void LookUpRate(float AxisValue);
	float BaseLookUpRate = 0.0f;

	void OpenFire();
	void CeaseFire();

	void StartAiming();
	void StopAiming();

	void StartRunnning();
	void StopRunning();



	UFUNCTION(BlueprintCallable)
		virtual void RecieveAnyDamage (float Damage, const class UDamageType* Damagetype, class AController* InstigatedBy, AActor* DamageCauser, bool Causer_TeamA);



	UFUNCTION(BlueprintCallable)
		void KillChar();

	UFUNCTION(BlueprintCallable)
		void ChangeTeam();
};
