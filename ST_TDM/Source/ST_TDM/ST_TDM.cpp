// Copyright Epic Games, Inc. All Rights Reserved.

#include "ST_TDM.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ST_TDM, "ST_TDM" );
