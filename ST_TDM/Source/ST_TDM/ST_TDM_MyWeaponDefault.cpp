// Fill out your copyright notice in the Description page of Project Settings.

#include "DrawDebugHelpers.h"
#include "ST_TDM_MyWeaponDefault.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AST_TDM_MyWeaponDefault::AST_TDM_MyWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;
	
	WeaponSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponSKeletalMesh"));
	WeaponSkeletalMesh->SetGenerateOverlapEvents(false);
	WeaponSkeletalMesh->SetCollisionProfileName("NoCollision");
	WeaponSkeletalMesh->SetupAttachment(SceneComponent);
	
	
	WeaponStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponStaticMesh"));
	WeaponStaticMesh->SetGenerateOverlapEvents(false);
	WeaponStaticMesh->SetCollisionProfileName("NoCollision");
	WeaponStaticMesh->SetupAttachment(SceneComponent);
	
	
	MuzzleLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Shoot Direction"));
	MuzzleLocation->SetupAttachment(RootComponent);
	
}

// Called when the game starts or when spawned
void AST_TDM_MyWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AST_TDM_MyWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
}

//��������� �������� ����� ��������� ������ � ��������� ������ ������� � ���������.

void AST_TDM_MyWeaponDefault::FireTick(float DeltaTime)
{	
	if(bAmmoInMagazineLeft || bRoundInChamber)
	{
		TimeBetweenFire = TimeBetweenFire - DeltaTime;

		if (TimeBetweenFire <= 0)
		{
			bReadyToFire = true;
			OpenFire();
		}
	}
}


void AST_TDM_MyWeaponDefault::ReloadTick(float DeltaTime)
{
	if (bWeaponReloading)
	{
		ReloadTimer = ReloadTimer - DeltaTime;

		if (ReloadTimer <= 0.0f)
		{
			FinishReload();
		}

	}
}

void AST_TDM_MyWeaponDefault::DispersionTick(float DeltaTime)
{
}



void AST_TDM_MyWeaponDefault::OpenFire()
{
	if (bRoundInChamber && bReadyToFire && bRecievedFireCommand && !bWeaponReloading)
	{
		if(MuzzleLocation)
		{
			FVector ProjectileSpawnLocation = MuzzleLocation->GetComponentLocation();

			FRotator ProjectileSpawnRotation = MuzzleLocation->GetComponentRotation();

			if (bTraceCheckLogic)
			{
				FHitResult TraceHit;
				FVector Start = MuzzleLocation->GetComponentLocation();
				FVector End = Start +  MuzzleLocation->GetForwardVector() * 2000.0f;
				TArray<AActor> IngoredActors;
				FCollisionQueryParams Params;
				FCollisionResponseParams RespParams;

				if (GetWorld()->LineTraceSingleByChannel(TraceHit, Start, End,
					ECC_Visibility, Params, RespParams))
				{
					UGameplayStatics::ApplyPointDamage(TraceHit.GetActor(),
						DamageOfWeapon, Start, TraceHit,
						NULL, this, NULL);
				}
				DrawDebugLine(GetWorld(), Start, End, FColor::Red, false, 5.0f, 0.f, 5.0f);
			}
		}
	}
}

void AST_TDM_MyWeaponDefault::StartReload()
{
	bWeaponReloading = true;
	bAmmoInMagazineLeft = 0;

}

void AST_TDM_MyWeaponDefault::FinishReload()
{
	ReloadTimer = ReloadTime;

	bWeaponReloading = false;

	bRoundInChamber = true;

	AmmoInMagazineLeft = MaxAmmoInMagazine;
}
