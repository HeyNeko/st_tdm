// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "ST_TDM_MyWeaponDefault.generated.h"

UCLASS()
class ST_TDM_API AST_TDM_MyWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AST_TDM_MyWeaponDefault();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* WeaponSkeletalMesh = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* WeaponStaticMesh = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* MuzzleLocation = nullptr;




protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:

	UPROPERTY(VisibleAnywhere)
		int MaxAmmoInMagazine = 30;

	UPROPERTY(VisibleAnywhere)
		int CurrentAmmoInMagazine = 30;

	UPROPERTY(VisibleAnywhere)
		float DamageOfWeapon = 0.0f;

	UPROPERTY(VisibleAnywhere)
		float ReloadTime = 1.0f;

	UPROPERTY(VisibleAnywhere)
		float ShotsPerMinute = 100;
	
	/**/
	float TimeBetweenFire = 60 / ShotsPerMinute;
	
	float FireTimer = TimeBetweenFire;

	float ReloadTimer = ReloadTime;

	float AmmoInMagazineLeft = 0.0f;

	/**/
	bool bRoundInChamber = false;

	bool bReadyToFire = true;

	bool bRecievedFireCommand = false;

	bool bWeaponReloading = false;

	bool bAmmoInMagazineLeft = false;

	bool bTraceCheckLogic = true;


	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()

		void FireTick(float DeltaTime);
		
		void ReloadTick(float DeltaTime);

		void DispersionTick(float DeltaTime);

		void StartReload();

		void FinishReload();

		void OpenFire();
};
