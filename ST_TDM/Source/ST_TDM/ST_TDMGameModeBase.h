// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ST_TDMGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ST_TDM_API AST_TDMGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
