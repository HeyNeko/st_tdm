// Fill out your copyright notice in the Description page of Project Settings.
#include "Net/UnrealNetwork.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "ST_TDM_MyChar.h"


// Sets default values
AST_TDM_MyChar::AST_TDM_MyChar()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseTurnAtRate = 45.0f;
	BaseLookUpRate = 45.0f;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 100.0f, 0.0f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

}

// Called when the game starts or when spawned
void AST_TDM_MyChar::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AST_TDM_MyChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CurrentVelocityX != 0 || CurrentVelocityY != 0)
	{
		AddMovementInput(FVector(CurrentVelocityX, CurrentVelocityY, 0.0f), 1.0f);
	}

	if (CurrentLookX != 0 || CurrentLookY != 0)
	{
		(FVector(CurrentLookX, CurrentLookY, 0.0f), 1.0f);
	}

}

/*������� ��������� � ���������*/
// Called to bind functionality to input
void AST_TDM_MyChar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("OpenFire", IE_Pressed, this, &AST_TDM_MyChar::OpenFire);
	PlayerInputComponent->BindAction("OpenFire", IE_Released, this, &AST_TDM_MyChar::CeaseFire);

	PlayerInputComponent->BindAction("Aim", IE_Pressed, this, &AST_TDM_MyChar::StartAiming);
	PlayerInputComponent->BindAction("Aim", IE_Released, this, &AST_TDM_MyChar::StopAiming);

	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &AST_TDM_MyChar::StartRunnning);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &AST_TDM_MyChar::StopRunning);
	
	PlayerInputComponent->BindAxis("MoveForward", this, &AST_TDM_MyChar::Move_Forward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AST_TDM_MyChar::Move_Right);

	PlayerInputComponent->BindAxis("LookUp", this, &AST_TDM_MyChar::LookUpRate);
	PlayerInputComponent->BindAxis("LookRight", this, &AST_TDM_MyChar::TurnAtRate);



}


void AST_TDM_MyChar::Move_Forward(float AxisValue)
{
	if (AxisValue != 0.0f)
	{
		AddMovementInput(GetActorForwardVector(), AxisValue);
	}
}

void AST_TDM_MyChar::Move_Right(float AxisValue)
{
	if (AxisValue != 0.0f)
	{
		AddMovementInput(GetActorRightVector(), AxisValue);
	}
}

void AST_TDM_MyChar::LookUpRate(float AxisValue)
{
	AddControllerPitchInput(AxisValue * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AST_TDM_MyChar::TurnAtRate(float AxisValue)
{
	AddControllerYawInput(AxisValue * BaseTurnAtRate * GetWorld()->GetDeltaSeconds());
}


/* ������� ��������� �� ���������*/
void AST_TDM_MyChar::OpenFire()
{
	IsFiring = true;

}

void AST_TDM_MyChar::CeaseFire()
{
	IsFiring = false;
}

void AST_TDM_MyChar::StartAiming()
{
	IsAiming = true;
}

void AST_TDM_MyChar::StopAiming()
{
	IsAiming = false;
}

void AST_TDM_MyChar::StartRunnning()
{
	IsRuning = true;
}

void AST_TDM_MyChar::StopRunning()
{
	IsRuning = false;
}

/*������� ��������� � ���������� �����*/
void AST_TDM_MyChar::RecieveAnyDamage(float Damage, const class UDamageType* Damagetype, class AController* InstigatedBy, AActor* DamageCauser, bool Causer_TeamA)
{
	if (Causer_TeamA != TeamA)
	{
		Super::ReceiveAnyDamage(Damage, Damagetype, InstigatedBy, DamageCauser);

		CurrentHealth = CurrentHealth - Damage;

		if (CurrentHealth <= 0)
		{
			KillChar();
		}
	}
}

void AST_TDM_MyChar::KillChar()
{
	CharIsAlive = false;
	//APlayerController* MyPC = Cast<APlayerController>(,);
}

/*����������� �������*/
void AST_TDM_MyChar::ChangeTeam()
{
	TeamA = !TeamA;
}

void AST_TDM_MyChar::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AST_TDM_MyChar, CurrentHealth);
}